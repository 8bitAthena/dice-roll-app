# Dice Roll App

## Overview

This is my first flutter project (so please be nice). It's a basic app in which  
you are able to press a button and roll a dice any number of times.

### Concepts learnt

- The flutter tree structure for representing widget hierarchy.
- Creating reusable widget trees and encapsulating them as Objects (classes).
- The difference between stateless and stateful widgets and how this impacts rendering.
- How to optomise rendering by different variable placement to minimise redundant  
object creation.
