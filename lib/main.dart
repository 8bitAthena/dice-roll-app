import 'package:flutter/material.dart';
import 'gradient_container.dart';

void main() {
  runApp(
    MaterialApp(
      home: Scaffold(
        backgroundColor: Colors.deepPurple,
        body: GradientContainer(
          const [
            Color.fromARGB(255, 23, 65, 10),
            Color.fromARGB(255, 96, 194, 67),
          ],
        ),
      ),
    ),
  );
}
