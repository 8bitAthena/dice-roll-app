// import 'package:first_app/styled_text.dart';
import 'package:first_app/dice_roller.dart';
import 'package:flutter/material.dart';

const startAlignment = Alignment.topLeft;
const endAlignment = Alignment.bottomRight;
// const tells dart that var is compile time constant, unlike final which is
// used if for example "final startAlignment = getAlignment();" relies on
// function return

class GradientContainer extends StatelessWidget {
  // first key refer to var called key in super class Stateless Widget
  // second is ref to my argument key.
  // Can write GradientContainer({super.key}); as short cut to naming key var
  // of this class to the value of the key var in the super class.
  // Const implies to dart that this is a class that can be reused and stored in
  // memory.
  const GradientContainer(this.colors, {key}) : super(key: key);

  //make second optional constructor, can't be const as list is mutable
  GradientContainer.purple({super.key})
      : colors = [Colors.deepPurple, Colors.indigo];

  // lists by default in dart can be edited
  final List<Color> colors;

  @override
  Widget build(context) {
    // give flutter access to the widget you want rendered
    // at the position you want it rendered at.
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: colors,
          begin: startAlignment,
          end: endAlignment,
        ),
      ),
      child: const Center(
        child: DiceRoller(),
      ),
    );
  }
}
